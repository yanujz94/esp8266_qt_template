#!/bin/bash
killall putty
make
make flash

device=$(esptool.py  chip_id | grep "Serial port" |cut -d' ' -f 3)
if [ $(echo $device | wc -w) = 0 ];then
	echo "No device found"
	exit
fi
putty -serial $device -sercfg 115200&
sleep 0.2
PUTTY_ID=$(xdotool search --onlyvisible --name Putty)
xdotool windowsize $PUTTY_ID 350 1000
xdotool windowmove $PUTTY_ID 1700 0

