# My makefile
SKETCH		 = {filename}
UPLOAD_PORT 	 = /dev/ttyUSB0
BOARD 		 = esp210
F_CPU 		?= 80000000L
FLASH_MODE 	?= qio
FLASH_SPEED 	?= 40
UPLOAD_RESET 	?= ck
UPLOAD_SPEED 	?= 1500000
COMP_WARNINGS	?= -w
INCLUDE_VARIANT  = generic

include ./makeEspArduino.mk
